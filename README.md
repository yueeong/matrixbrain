## Matrix on my Mind.
This library provides two functions to perform matrix transposition and matrix multiplication in C++.

* The library provides a PrintMatrix function to output the matrix to console.

* Transpose and MultiplyMatrix to transpose the matrix and to multiply 2 matrices respectively.

* Takes input parameter matrices in the form of vector of vectors

* GetInputMatrix function for user to input matrix from command line console.

## Usage
Include "matrix_util.h" as a header.

To get input from console, call GetInputMatrix 
```c++
vector<vector<double>> input_matrix = GetInputMatrix();
Enter number of rows of matrix: 2
Enter number of cols of matrix: 2

Enter matrix elements:
 Row: 0
 Column: 0 = 1
 Column: 1 = 2
 Row: 1
 Column: 0 = 3
 Column: 1 = 4
```


#### Transpose
```c++
vector<vector<double>> to_transpose(2, vector<double>(3));
.
. .. fill matrix elements either by calling GetInputMatrix or 
. .. fill in with calling function.
.
.
vector<vector<double> > mt = Transpose(to_transpose);
PrintMatrix(mt);
```
Example Output:
```
Matrix output: 
0.1 0.2 0.3 
1.1 1.2 1.3 
```

#### Multiply Matrices
```c++
vector<vector<double>> result = MultiplyMatrix(matrix1, matrix2);
PrintMatrix(result);
```
Example Output:
```
Matrix output: 
0.86 0.92 0.98 
4.16 4.52 4.88 
```


#### To compile : 
```bash
> g++ -std=c++11 main.cpp 
```

#### To run tests:
```bash
> cd tests
> g++ -std=c++11 tests.cpp && ./a.out
```

#### To compile and run main in project root:
```
> g++ -std=c++11 main.cp && ./a.out
```