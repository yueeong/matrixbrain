#include <iostream>
#include <vector>
#include "matrix_util.h"
using namespace std;

int main() 
{   
    // Matrix Transpose
    vector<vector<double>> to_transpose(2, vector<double>(3));
    // Row 1, 3 columns
    to_transpose[0][0] = 0.1;
    to_transpose[0][1] = 0.2;
    to_transpose[0][2] = 0.3;
    // Row 2, 3 columns
    to_transpose[1][0] = 1.1;
    to_transpose[1][1] = 1.2;
    to_transpose[1][2] = 1.3;

    vector<vector<double> > mt = Transpose(to_transpose);

    PrintMatrix(to_transpose);
    PrintMatrix(mt);

    // Matrix Multiply
    vector<vector<double>> matrix1(2, vector<double>(3));
    vector<vector<double>> matrix2(3, vector<double>(3));
    // matrix1 2x3
    matrix1[0][0] = 0.1; matrix1[0][1] = 0.2; matrix1[0][2] = 0.3;
    matrix1[1][0] = 1.1; matrix1[1][1] = 1.2; matrix1[1][2] = 1.3;

    //matrix2 3x3
    matrix2[0][0] = 0.1; matrix2[0][1] = 0.2; matrix2[0][2] = 0.3;
    matrix2[1][0] = 1.1; matrix2[1][1] = 1.2; matrix2[1][2] = 1.3;
    matrix2[2][0] = 2.1; matrix2[2][1] = 2.2; matrix2[2][2] = 2.3;

    vector<vector<double>> result = MultiplyMatrix(matrix1, matrix2);

    PrintMatrix(result);

    cout << endl << "--------------------------------------------" << endl;
    cout << endl << "%%%% Input Matrix from console manually %%%%" << endl;
    cout << endl << "--------------------------------------------" << endl;
    cout << "First Matrix: " << endl;
    vector<vector<double>> t1 = GetInputMatrix();
    PrintMatrix(t1);

    cout << "Second Matrix: " << endl;
    vector<vector<double>> t2 = GetInputMatrix();
    PrintMatrix(t2);

    vector<vector<double>> t3 = MultiplyMatrix(t1, t2);
    PrintMatrix(t3);

}