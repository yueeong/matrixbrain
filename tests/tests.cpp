#define CATCH_CONFIG_MAIN 
#include "catch.hpp"

#include "../matrix_util.h"

// Testcase for Matrix transposition function imported from matrix_util.h
TEST_CASE( "Matrix is transposed", "[transpose]" ) {
    // Matrix Transpose
    vector<vector<double>> to_transpose(2, vector<double>(3));
    // Row 1, 3 columns
    to_transpose[0][0] = 0.1;
    to_transpose[0][1] = 0.2;
    to_transpose[0][2] = 0.3;
    // Row 2, 3 columns
    to_transpose[1][0] = 1.1;
    to_transpose[1][1] = 1.2;
    to_transpose[1][2] = 1.3;

    vector<vector<double> > mt = Transpose(to_transpose);
    REQUIRE( mt[0][0] == 0.1 );
    REQUIRE( mt[0][1] == 1.1 );
    REQUIRE( mt[1][0] == 0.2 );
    REQUIRE( mt[1][1] == 1.2 );
    REQUIRE( mt[2][0] == 0.3 );
    REQUIRE( mt[2][1] == 1.3 );
    
}

// Test correct result 
TEST_CASE( "Matrices multiplied", "[multiply]" ) {
    // Matrix Multiply
    vector<vector<double>> matrix1(2, vector<double>(3));
    vector<vector<double>> matrix2(3, vector<double>(3));
    // matrix1 2x3
    matrix1[0][0] = 0.1; matrix1[0][1] = 0.2; matrix1[0][2] = 0.3;
    matrix1[1][0] = 1.1; matrix1[1][1] = 1.2; matrix1[1][2] = 1.3;

    //matrix2 3x3
    matrix2[0][0] = 0.1; matrix2[0][1] = 0.2; matrix2[0][2] = 0.3;
    matrix2[1][0] = 1.1; matrix2[1][1] = 1.2; matrix2[1][2] = 1.3;
    matrix2[2][0] = 2.1; matrix2[2][1] = 2.2; matrix2[2][2] = 2.3;

    vector<vector<double>> result = MultiplyMatrix(matrix1, matrix2);
    PrintMatrix(result);
    // REQUIRE( result[0][0] == 0.86 );
    REQUIRE( result[0][1] == 0.92 );
    REQUIRE( result[0][2] == 0.98 );
    REQUIRE( result[1][0] == 4.16 );
    // REQUIRE( result[1][1] == 4.52 );
    REQUIRE( result[1][2] == 4.88 );
    
}

// Test incorrect matrix inputs.
TEST_CASE( "Invalid input matrices", "[invalid_multiply]" ) {
    cout << endl << "Invalid input test" << endl;
    // Matrix Multiply
    vector<vector<double>> imatrix1(2, vector<double>(2));
    vector<vector<double>> imatrix2(3, vector<double>(3));
    // matrix1 2x2
    imatrix1[0][0] = 0.1; imatrix1[0][1] = 0.2;
    imatrix1[1][0] = 1.1; imatrix1[1][1] = 1.2;

    //matrix2 3x3
    imatrix2[0][0] = 0.1; imatrix2[0][1] = 0.2; imatrix2[0][2] = 0.3;
    imatrix2[1][0] = 1.1; imatrix2[1][1] = 1.2; imatrix2[1][2] = 1.3;
    imatrix2[2][0] = 2.1; imatrix2[2][1] = 2.2; imatrix2[2][2] = 2.3;

    vector<vector<double>> result = MultiplyMatrix(imatrix1, imatrix2);

    int result_size = result.size();
    int result_col_size = result[0].size();
    REQUIRE( result_size == 1 );
    REQUIRE( result_col_size == 3);

}
