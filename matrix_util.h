#include <vector>

using namespace std;

// Function GetInputMatrix: Queries user for matrix to be input from console command line.
// Inputs parameters:
//  - none
// Usage:
//  Eg: vector<vector<double>> t = GetInputMatrix();
// Sample Output:
//  Enter number of rows of matrix: 2
//  Enter number of cols of matrix: 2
//
// Enter matrix elements:
//  Row: 0
//  Column: 0 = 1
//  Column: 1 = 2
//  Row: 1
//  Column: 0 = 3
//  Column: 1 = 4
//
vector<vector<double>> GetInputMatrix()
{   
    int rows;
    int cols;
    cout << "Enter number of rows of matrix: ";
    cin >> rows;
    cout << "Enter number of cols of matrix: ";
    cin >> cols;

    vector<vector<double>> input_matrix(rows, vector<double>(cols));

    cout<< endl << "Enter matrix elements: " << endl;
        for(int i=0; i<rows; i++) {
            cout << " Row: " << i << endl;
            for(int j=0; j<cols; j++) {
                cout << " Column: " << j << " = ";
                cin >> input_matrix[i][j];
            }
        }       

    return input_matrix;
}




// Function PrintMatrix: Prints out matrix taking vector of vectors as input matrix format
// Inputs:
//  - matrix : matrix to be printed
// Usage : 
//  Eg : PrintMatrix(matrix_to_be_printed)
// Sample Output : 
//  Matrix output: 
//  0.1 1.1 2.1 3.1 
//  0.2 1.2 2.2 3.2 
//  0.3 1.3 2.3 3.3 
//
void PrintMatrix(vector<vector<double>>& matrix)
{
    cout << endl << "Matrix output: " << endl;
    for (int i=0 ; i < matrix.size(); i++) // Loop for each row
    {
        for (int j=0; j < matrix[i].size(); j++)
        {
            cout << matrix[i][j] << " "; // Print to console each column
        }
        cout << endl;
    }
}

// Description : Transposes given matrix to matrix_t
// Inputs : 
//  - matrix : to be transposed
// Returns :
//  vector<vector<double>>
// Usage :
//  vector<vector<double>> result = Transpose(matrix)
//
vector<vector<double>> Transpose(vector<vector<double>> matrix)
{
    cout << "Transposing." << endl;
    cout << "Original Rows: " << matrix.size() << endl;
    cout << "Original Cols: " << matrix[0].size() << endl;

    // initialize transformed matrix dimensions.
    vector<vector<double> > matrix_t(matrix[0].size(), vector<double>(matrix.size()));

    // nested loop through vector of vectors
    for (int i=0 ; i < matrix.size(); i++) { // Loop for each row
        for (int j=0; j < matrix[i].size(); j++) {
            matrix_t[j][i] = matrix[i][j] ; // Swap col-row assignment to transpose each element
        }
    }
    cout << "End transpose" << endl;
    return matrix_t;
}

// Description : Multiplies 2 matrices
// Inputs : 
//  - matrix1 : first matrix
//  - matrix2 : second matrix
// Returns :
//  vector<vector<double>>
// Usage :
//  vector<vector<double>> result = MultiplyMatrix(matrix1, matrix2);
//
vector<vector<double>> MultiplyMatrix(vector<vector<double>> matrix1, 
                                    vector<vector<double>> matrix2)
{
    int matrix1_rows = matrix1.size();
    int matrix1_cols = matrix1[0].size();
    int matrix2_rows = matrix2.size();
    int matrix2_cols = matrix2[0].size();

    cout << endl << "First matrix dimensions: ";
    cout << matrix1_rows << "x" << matrix1_cols << endl;
    cout << "Second matrix dimensions: ";
    cout << matrix2_rows << "x" << matrix2_cols << endl;

    // Declare result of the matrix multiplication
    vector<vector<double>> product_matrix;

    // Check valid matrix dimenstions for multiplication
    if (matrix1_cols != matrix2_rows) {
        cout << endl << "**** Matrix size requirements not met. ****" << endl;
        cout << "**** Matrix first row has been filled with {0.0, 0.0, 0.0} ****" << endl;
        product_matrix.push_back({0.0, 0.0, 0.0});
        return product_matrix;
    }
    else {
        product_matrix.resize(matrix1_rows); // vector resize of resulting matrix rows
        for (int i=0; i < matrix1_rows; i++ ) {
            product_matrix[i].resize(matrix2_cols); // vector resize columns of result matrix
            for (int j=0; j < matrix2_cols; j++) {
                product_matrix[i][j] = 0.0; 
                for (int k=0; k < matrix1_cols; k++ ) {
                    // loop to calculate each element of the result matrix
                    product_matrix[i][j] = product_matrix[i][j] + (matrix1[i][k]*matrix2[k][j]);
                }
            }
        }
        return product_matrix;
    }
}